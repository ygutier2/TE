#!/bin/bash

cwd=$(pwd)
ff=$cwd/MASTERFILES

LAMMPSparallel="${cwd}/../lmp_mpi_23Jun2022"
lscript="lammps_cgnetwork"

#Number of replicas to run
nr=1

#USER: Define some variables
Nstars="350"
rho="0.02"



for i in $( seq 1 ${nr} )
do 
  mkdir REP${i}
  cd    REP${i}

  cp ${ff}/${lscript} .
  
  #Copy equilibrated configuration here
  cp ${cwd}/../0.InitialConfigurations/Equilibrated.Nstars${Nstars}.rho${rho}.data .
  
  #Create random seed numbers that can be used in LAMMPS 
  r1=$(od -An -N2 -i /dev/urandom)
  r2=$(od -An -N2 -i /dev/urandom)
  r3=$(od -An -N2 -i /dev/urandom)
  echo "variable seed equal ${r1}"        >  parameters.dat
  echo "variable seedthermo  equal ${r2}" >> parameters.dat

  #Replace the lines in the LAMMPS script with the Nstars and rho values here
  sed -i "s/variable Nstars equal 175/variable Nstars equal ${Nstars}/g" ${lscript}
  sed -i "s/variable rho equal 0.01/variable rho equal ${rho}/g"         ${lscript}
  
  mpirun -np 2 ${LAMMPSparallel} -in ${lscript}
  
  cd ../
done