# Simulation scripts

The script **bash.run.sh** provided in this folder contains the instructions to run simulations with the DNAns model. The bash script makes use of two files: 

* The LAMMPS script **lammps_cgnetwork** with the commands to run the simulation. This file is located inside the folder **MASTERFILES**.
* One of the equilibrated configurations located in *0.InitialConfigurations*.

Before running the simulation, the user needs to set two variables: 

> $Nstars$, with the number of DNAns in the system. It can be either: 175, 350, 525, 700, 875, 1050, 1400 or 1750.
>
> The corresponding volume fraction ($\rho$) of the system: 0.01, 0.02, 0.03, 0.04, 0.06, 0.08 or 0.1.

The default values are $Nstars=350$ and $\rho=0.02$. After setting the previous two variables, you can run simulations by simply executing the bash script: *./bash.run.sh*. Note, that we have assumed that the LAMMPS executable is called *lmp_mpi_23June2022* and it is located in the root folder of this repository. If the simulation runs successfully, a folder called *REP1* will be created. Inside this folder you will find:

* 1.- The file **gtTOT.data** which contains seven columns. The first one is the brownian timestep and the rest represent the six different components of the autocorrelation of the stress-tensor.
* 2.- The folder **data** in which the results from simulations are dumped. By default we create a single file every 10<sup>5</sup> timesteps and the simulation runs for a total of  10<sup>8</sup> timesteps. The generic name of these files is: *stars.Nstars350.rho0.02.* followed by the timestep (0, 100000, 200000, ...).