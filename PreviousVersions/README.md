# Analysis Tools versions
Here you will find previous versions of the code and a brief description of the modifications made.

* **1. Version 2023-08-18**: This is the repository used for the analysis shown with the initial submission of our manuscript. Since then, the code has been modified to increase the efficiency of the calculations.
* **2. Version 2024-04-12**: We replaced some of the c++ scripts by AWK scripts:

> *2023-08-18/2.Analysis/3\_ShortestPath/average\_at\_same\_l.cpp* ⮕ *2024-04-12/2.Analysis/3\_ShortestPath/awk.ave\_at\_same\_l.sh*
>
> *2023-08-18/2.Analysis/4\_MinimumLoops/average\_at\_same\_l.cpp* ⮕ *2024-04-12/2.Analysis/4\_MinimumLoops/awk.ave\_at\_same\_lmin.sh*
>
> *2023-08-18/2.Analysis/4\_MinimumLoops/minloop\_distribution.cpp*  ⮕ *2024-04-12/2.Analysis/4\_MinimumLoops/awk.Nminloop\_vs\_time.sh*

* **3 Version 2024-08-07**: We replaced **while read** loops by AWK loops. This increased considerably the efficiency of our codes:

> *2024-04-12/2.Analysis/3\_ShortestPath/bash\_fractalDimension.sh*   ⮕ *2024-08-07/2.Analysis/3\_ShortestPath/bash\_fractalDimension.sh*
>
> *2024-04-12/2.Analysis/4\_MinimumLoops/bash\_fractalDimMinLoops.sh* ⮕ *2024-08-07/2.Analysis/4\_MinimumLoops/bash\_fractalDimMinLoops.sh*

* We replaced mathematica notebooks (.nb files) by .m files, so the scripts can be run through the terminal using the Mathematica kernel.

> *2024-04-12/2.Analysis/1\_networkConnectivity/network\_connectivity.nb* ⮕ *2024-08-07/2.Analysis/1\_networkConnectivity/network\_connectivity.m*
>
> *2024-04-12/2.Analysis/2\_maxConnectedComponent/maxCComp.nb* ⮕ *2024-08-07/2.Analysis/2\_maxConnectedComponent/maxCComp.m*
>
> *2024-04-12/2.Analysis/3\_ShortestPath/MinPathBranching.nb* ⮕ *2024-08-07/2.Analysis/3\_ShortestPath/MinPathBranching.m*
>
> *2024-04-12/2.Analysis/4\_MinimumLoops/MinLoops.nb* ⮕ *2024-08-07/2.Analysis/4\_MinimumLoops/MinLoops.m*

* We replaced one of the c++ scripts by AWK scripts:

> *2024-04-12/2.Analysis/4\_MinimumLoops/remove\_repeated\_lopps.cpp* ⮕ *2024-08-07/2.Analysis/4_MinimumLoops/awk.remove\_repeated\_loops.sh*

* We added one file

> *2024-08-07/2.Analysis/1\_networkConnectivity/occurrences.awk*

* **4. Version 2024-09-16**: The difference with the previous version is that we modified the c++ script to compute the linking number between minimum loops **ComputeLinking.c++**.

> *LinkingMap* change from a 2D vector to an integer. This saves a lot of memory ram, particularly when dealing with several loops.
>
> The mn-elements of the linking matrix are now printed only when the linking between loops m and n is different from zero. This saves storage space.
>
> We don't perform the loop to reconstruct the second minimum loops starting from a different bead. Instead, we work-out a set of translations that allows us to compute linking only once between every pair of minimum loops. This saves computational time.

* **5. Version 2024-12-19**: This is the version included at the root folder of this repository:

> First, we modified the Mathematica script (**MinLoops.m**) to compute the minimum loops: in the previous version we used to find all loops passing through a vertex ($r_{1}$) and formed by at the most $l_{max}=20$ DNAns. Then, we selected the smallest of these loops. This is not efficient and therefore, this time we compute first all the loops formed by $l=l_{min}=4$ DNAns. If no loops are found, we continue our search (through the same vertex $r_{1}$) of loops made by l=5 DNAns. This process is repeated until we find at least one loop of any length $l\in[l_{min},l_{max}]$.
> 
> We also modified **fractalD_ring_v2.cpp**. In the previous version we used to impose a constraint in the radius of gyration of nanostars ($R_{g, min} < 0.3L$, where L is the size of the cubic box). This was necessary to exclude fake rings (linear chains linked through the PBC) in our analysis. Howevever, if we use a more general form of the Minimum Image Criteria (MIC) to reconstruct loops, then we can remove this condition and only ask that the end-to-end distance of reconstructed loops is smaller than the distance between consecutive beads in the arms of a DNAns.
> 
> The same general form of the MIC is implemented in **ComputeLinking.c++**.

