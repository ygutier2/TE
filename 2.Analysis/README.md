# Analysis

Inside the folder **CONFIGURATIONS** we provide configurations obtained from short simulations ($10^{6}$ timesteps) of systems at different volume fractions ($\rho$). The rest of the folders contain the instructions to perform the analysis and they should be run in order:

* **1_networkConnectivity:** we use a c++ programm to extract the moleculeIDs and patchesIDs of nanostars that are hybridized.
* **2_maxConnectedComponent:** we find the largest connected component as function of time.
* **3_ShortestPath:** we identify the branching points and the shortest path between them.
* **4_MinimumLoops** we identify unrepeated minimum loops in the graph and we compute the linking number between them.

Note that by default the analysis is run for the system with $N=350$ nanostars and at $\rho=0.02$.