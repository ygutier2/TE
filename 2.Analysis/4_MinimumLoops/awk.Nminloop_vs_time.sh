#!/bin/bash

data_file="fractalDimension2.dat"

awk '
{
    count[$1]++;   # Keep track of the number of minimum loops (with any length) at the same timestep (column 1)
}
END {
    for (key in count) {        
        print key, count[key];
    }
}
' "$data_file" > "NminLoops.txt"
