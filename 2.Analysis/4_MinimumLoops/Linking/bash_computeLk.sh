#!/bin/bash
  #The number of replicas
  nr=1
  
  #System details
  Nstars=350
  Nparticlesperstar=10
  rho="0.02"
  
  #Current working directory
  cwd=$(pwd)
  
  #IMPORTANT: the parameters here should be the same as the ones inthe mathematica notebook:
  ti=0
  dfreq=100000
  tf=100000  
  #The timestep at which you want to dump the linked configurations found by this analysis
  td=100000  
  
  
  #Compile the programm:
  c++ -std=c++11 ComputeLinking.c++ -o ComputeLinking

  #Create a folder in which to store the pair of rings that are linked
  if [ -f "LinkingMatrix.dat" ] ; then
    rm "LinkingMatrix.dat"
    rm "Linking_vs_time.dat"
    rm -r linkedConfigurations
    rm -r linkedIDS
    rm -r linkedRepeatedIDS
  fi
  
  mkdir -p linkedConfigurations
  mkdir -p linkedIDS
  mkdir -p linkedRepeatedIDS
  
  
  for i in $( seq 1 ${nr} )
  do
     for t in $( seq ${ti} ${dfreq} ${tf})
     do
        #1.- Path to the input data
        datapath="../../CONFIGURATIONS/rho${rho}/"
      
        #2.- Output path
        outputpath="./"
      
        #3.- Input file with atoms information (without timestep)
        rfile="stars.Nstars${Nstars}.rho${rho}."
    
       ./ComputeLinking ${datapath} ${outputpath} ${rfile} ${Nstars} ${Nparticlesperstar} ${t} ${td}
       
    done
  done
  
  rm ComputeLinking
