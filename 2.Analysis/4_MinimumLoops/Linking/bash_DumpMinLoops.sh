#!/bin/bash
#System details
  Nstars=350
  Nparticlesperstar=10
  rho=0.02
  
  #Distance cut-off to consider a contact between the patches of nanostars
  rc="0.2"
  
  #Current working directory
  cwd=$(pwd)

###########################################################
#Here we compute the particle ID of all beads forming loops
###########################################################
 #1.- Path to the input data
 datapath="../../CONFIGURATIONS/rho${rho}/"
      
 #2.- Output path
 mkdir -p dataMinloop
 outputpath="./dataMinloop/"
      
 #3.- Input file with atoms information (without timestep)
 rfile="stars.Nstars${Nstars}.rho${rho}."
 
 c++ MinLoopsIDs.cpp -o MinLoopsIDs.out -lm
 
 # READ Variables from the file copied above #
 #############################################
 awk -v dp="${datapath}" -v op="${outputpath}" -v rf="${rfile}" -v ns="${Nstars}" -v nppns="${Nparticlesperstar}" -v rc1="${rc}" '{
   #$1=timestep, $2=l
   arguments = dp" "op" "rf" "$1" "ns" "nppns" "rc1" "$2;
   
   for (i = 4; i <= NF; i++) {
     col[i] = $i;
     
     arguments = arguments" "col[i];   
   }
   
   #print "Arguments:", arguments;
   # Execute the C++ program and pass the concatenated values as a single argument
   cmd = "./MinLoopsIDs.out " arguments;
   system(cmd);
 }' ../fractalDimension2.dat
 
 rm MinLoopsIDs.out
 
 #This programm will create a file per timestep analysed  (MinLoopsParticleIDs_t*) with the following columns:
 #1.-The total number of particles (excluding patches) forming the minimum loop
 #2.-The ParticleID of consecutive particles in the loop
