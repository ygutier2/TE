#include<vector>     /*Library for using vector insted of arrays*/
#include<iostream>   /*Basic functions*/   
#include <cmath>     /*Define some fundamental mathematical operations*/
#include<stdlib.h>   /*Useful for random numbers*/
#include <sstream>
#include <fstream>   /*To read and write into files*/
#include <limits>
#include <stdio.h>   /*Basic functions in particular "sprintf"*/
#include <iomanip>      // std::setprecision
#include <tuple>     //Functions can return more than one value
#include <set>
#include <algorithm>



using namespace std;

#define pi 3.141592653589793238462643383279502884197169399375
#define SMALL 1e-20

//The timestep set in lammps
double dt=0.01;

int main(int argc, char *argv[])
{
    //cout << "ok" << endl;
    /*argv[1] Directory where the data is*/
    /*argv[2] Directory where the output will be stored*/
    /*argv[3] Input file*/
    
    /*timestep*/
    int timestep;
    timestep = atoi(argv[4]);
    
    /*Number of polymers in the simulation at t=0*/
    int M;
    M = atoi(argv[5]);
    
    /*Number of particles per DNAns*/
    int Nbeads;
    Nbeads = atoi(argv[6]);
    
    /*Number of patches per molecule*/
    int Np=3;
    
    /*Cutoff distance for a contact*/
    float rc;
    rc = atof(argv[7]);

    /*Number of molecules (DNAns) forming the minimum loop*/
    int l;
    l = atoi(argv[8]);
    
    /*Molecule-id of the DNAns connected, in total there are l*/
    vector <int> Molid(l);

    //Read the molecule ids
    for(int i=0; i<l; i++){
        Molid[i] = atoi(argv[9+i]);
        
        //We have to add 1, because the moleculeid we read is actually molid-1
        Molid[i]+=1;
        //cout << "Molid[" << i << "] " << Molid[i] << endl;
    }

    /*Particle-id of the DNAns connected*/
    vector <int> Particleid(l);
    for(int i=0; i<l; i++){
        Particleid[i]=10*(Molid[i]);
    }

    //Information of all the particles In the system
    vector< vector< vector<double> > > position(M, vector<vector<double> >(Nbeads, vector<double>(3)));
    vector< vector<int> > typevec(M, vector<int>(Nbeads));
    vector< vector<int> > indice(M, vector<int>(Nbeads));
        
    //Information of all the particles forming the loop      
    vector< vector< vector<double> > > posLoop(l, vector<vector<double> >(Nbeads, vector<double>(3)));
    vector< vector<int> > typeLoop(l, vector<int>(Nbeads));
    vector< vector<int> > indiceLoop(l, vector<int>(Nbeads));
    
    //Only position and atomID of patches of the molecules forming the ring
    vector< vector< vector<double> > > posPatches(l, vector<vector<double> >(Np, vector<double>(3)));
    vector< vector<int> > index(l, vector<int>(Np));

    
    
    /*the following variables will be useful to read the data file*/
    long long int tbrownian;
    long long int time;
    long int Ntot;        //The total number of beads in the system. It should be equal to M*Nbeads.
    long int id,type,mol;
    double   x,y,z;
    int ix,iy,iz;
    double lmin,lmax,Lx,Ly,Lz;    
    int nm;
    
    /*Read the file with ids of terminal beads*/
    ifstream indata1;
    char readFile1[400] = {'\0'};
    string dummy;
    string line;
    sprintf(readFile1, "%s%s%d",argv[1],argv[3],timestep);
    indata1.open(readFile1);
    
    if (indata1.is_open())
    {       
        //Read headers
        for(int i=0;i<10;i++){
            if(i==1) {
                indata1 >> time;
                //cout << "time " << time <<endl;
                    
                tbrownian = time*dt;
                //cout << "Brownian time " << tbrownian <<endl;
            }

            if(i==3) {
                indata1 >> Ntot;
                //cout << "Nbonds " << Nbonds << endl;
            }

            if(i==5) {
                indata1 >> lmin >> lmax;
                Lx = lmax-lmin;
                //cout << "Lx " << Lx <<endl;
            }

            if(i==6) {
                indata1 >> lmin >> lmax;
                Ly = lmax-lmin;
                //cout << "Ly " << Ly <<endl;
            }

                if(i==7) {
                indata1 >> lmin >> lmax;
                Lz = lmax-lmin;
                //cout << "Lz " << Lz<<endl;
            }

                else getline(indata1,dummy);
        }
                
        //READ all ATOMS
        for(int n=0; n<Ntot; n++){
            indata1 >> id >> type >> x >> y >> z >> ix >> iy >> iz;
            
            int m = floor((double)(id-1)/(1.0*Nbeads));
            
            position[m][(id-1)%Nbeads][0] = x;
            position[m][(id-1)%Nbeads][1] = y;
            position[m][(id-1)%Nbeads][2] = z;
            
            typevec[m][(id-1)%Nbeads] = type;
            indice[m][(id-1)%Nbeads] = id;
        }
        
        indata1.close();        
        indata1.clear();
        
    }
/*   
    //else {cout << "Error opening file" << endl;}   
    for(int m=0; m<M; m++){
        for(int n=0; n<Nbeads;n++){
           cout << indice[m][n] << " " << typevec[m][n] << " " << position[m][n][0] << " " << position[m][n][1] << " " << position[m][n][2] << endl;
        }
    }
*/   
   
    ////////////////////////////////////////////////////////////////
    //Extract information of beads that are part of a Minimum loop//
    ////////////////////////////////////////////////////////////////
    for(int i=0; i<l; i++){       
        int flag1=0;
        for(int m=0; m<M; m++){
            int idx=0;
            if(m+1==Molid[i]){
                for(int n=0; n<Nbeads; n++){
                    flag1+=1;
                   
                    posLoop[i][n][0]=position[m][n][0];
                    posLoop[i][n][1]=position[m][n][1];
                    posLoop[i][n][2]=position[m][n][2];
                    typeLoop[i][n]  =typevec[m][n];
                    indiceLoop[i][n]=indice[m][n];
                }
            }
            if(flag1==10){break;}//Exit loop over M molecules
        }
    }
    
    ////////////////////////////////////////////////////////////////
    //Extract information of patches that are part of a Minimum loop//
    ////////////////////////////////////////////////////////////////
    for(int m=0; m<l; m++){
        int idx=0;
        for(int n=0; n<Nbeads;n++){
            if(typeLoop[m][n]==2){
                for(int d=0; d<3;d++){
                    posPatches[m][idx][d] = posLoop[m][n][d];
                    index[m][idx]         = indiceLoop[m][n];
                }
                idx+=1;
            }
        }
    }


/*    
    for(int m=0; m<M; m++){
        for(int n=0; n<Np;n++){
           cout << Nbeads*m+3*(n+1) << " " << posPatches[m][n][0] << " " << posPatches[m][n][1] << " " << posPatches[m][n][2] << endl;
        }
    }       
*/
   
    ///////////////////////////////////////////////////////////////////////////////////////
    // COMPUTE The id of the patches in contact between consecutive DNAns forming the loop
    //////////////////////////////////////////////////////////////////////////////////////
    double dx,dy,dz, dist;
    double x1, y1,z1;
    int   id1, m1;
    //The two pacthes connectig two stars
    vector<int> patch1;
    vector<int> patch2;
    for(int m=0; m<l; m++){
        //The position of the pacthes in the m-th DNAns
        for(int n=0; n<Np; n++){
            x=posPatches[m][n][0];
            y=posPatches[m][n][1];
            z=posPatches[m][n][2];
            id=index[m][n];
    
            //The position of patches in the consecutive DNAns
            for(int n1=0; n1<Np; n1++){
                if(m<l-1){
                    m1=m+1;
                    x1=posPatches[m1][n1][0];
                    y1=posPatches[m1][n1][1];
                    z1=posPatches[m1][n1][2];
                    id1=index[m1][n1];
                }
                
                if(m==l-1){
                    m1=0;
                    x1=posPatches[m1][n1][0];
                    y1=posPatches[m1][n1][1];
                    z1=posPatches[m1][n1][2];
                    id1=index[m1][n1];
                }
                
                dx=x-x1;
                dy=y-y1;
                dz=z-z1;
                    
                dist = sqrt(dx*dx+dy*dy+dz*dz);
                
                if(dist<rc){
                    //The two DNAns (m and m1) are connected through the following patches
                    patch1.push_back(id);
                    patch2.push_back(id1);
                
                }
            }
        }             
    }
    
       
    /////////////
    // PRINT The id of all the beads (not patches) connected to a loop
    /////////////
    char writeFile2[400] = {'\0'};
    sprintf(writeFile2, "%sMinLoopsParticleIDs_t%d.dat",argv[2],timestep);
    
    ofstream write2;
    write2.open(writeFile2, std::ios_base::app);
    
    for(int m=0; m<l; m++){
        //The total number of particles forming the loop
        int Nt=l*5;
        if(m==0){write2 << Nt << " ";}
        
        //DNAns core1
        int Core1=Particleid[m];
        
        //Two beads along the arm that is connected (and part of the first DNAns)
        int A1=patch1[m]-2; //This is the bead closer to the core
        int A2=patch1[m]-1; //This is the bead closer to the patch
        
        //Two beads along the arm that is connected (and part of the second DNAns)
        int B1=patch2[m]-1; //This is the bead closer to the patch
        int B2=patch2[m]-2; //This is the bead closer to the core
        
        if(m<l-1){write2 << Core1 << " " << A1 << " " << A2 << " " << B1 << " " << B2 << " ";}
        
        if(m==l-1){write2 << Core1 << " " << A1 << " " << A2 << " " << B1 << " " << B2;}
        
        
    }
    
    write2 << endl;
    write2.close();
    write2.clear();


return 0;
}
