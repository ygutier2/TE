# Linking number analysis
**Important:** The file *fractalDimension2.dat* (produced by the analysis in the folder above) is needed for the computation of the linking number.

The bash script (**bash\_DumpMinLoops.sh**) contains the instructions to read the moleculeID of all particles in the minimumu loops (from *fractalDimension2.dat*) and find the particleID of all beads in the minimum loop. After runing the script, this information is stored inside the folder *dataMinloop*. There, you will find a file per timestep (called *MinLoopsParticleIDs\_t0.dat*, *MinLoopsParticleIDs\_t100000.dat*, ...) containing in the first column the number of beads in the minimum loop, and in the following columns the particleIDs of all beads (no patches) in that minimum loop.

A second bash script (**bash\_computeLk.sh**) contains the instructions to run the the C++ code **ComputeLinking.c++**, which reads the particleIDs of beads in the minimum loops obtained previously, and also the snapshots from simulations in the folder *../../CONFIGURATIONS*. **IMPORTANT:** the variables $t_{i}$, $t_{f}$ and $d_{freq}$ of this section (lines 14-16) should be the same as the ones used in the mathematica notebook found in the folder above (*MinLoops.m*). This analysis will create two files:

*Linking\_vs\_time.dat*, which contains five columns:
> 1.- The timestep of the calculation.
>
> 2.- The total number of minimum loops.
>
> 3.- The sum of the absolute value of the linking number between all pairs of minimum loops.

*LinkingMatrix.dat* with four columns representing:
> 1.- The timestep.
>
> 2-3.- The rindIDS of the two rings used for the calculation of the linking number.
>
> 4.- The value of the linking number found between the two previous rings.

We note that in the bash script **bash\_computeLk.sh** and additional variable $t_{d}$ appears in line 18. The user needs to set this variable with the timestep at which additional information is printed. Setting the variable to $t_{d}=-1$ stops the printing of information. By default $t_{d}=100000$, so this analysis creates three folders:

> 1.- *linkedConfigurations*: At time $t_{d}$, a file per pair of linked rings will be created in this directory. For instance: LinkedConfigs\_100000\_ring1-5\_ring2-14.xyz describes that at timestep 100000, rings 5 and 14 are linked. We can open this file with vmd to see the reconstructed configuration of these rings through PBC and check that the analysis was successful (the loops are indeed linked).
>
> 2.- *linkedIDS*: At time $t_{d}$, a unique file is created, for instance "LinkedIDs\_100000.dat". This file contains which rings are linked, and the particle IDs of the beads of the two linked rings. With this information it is possible to visualize the linked rings in the original simulation with VMD, just follow these steps in a terminal:
>
>>2.1) Use vmd to open the trajectory from simulation. Assuming that the terminal path is located in this folder you have to type: *vmd ../../CONFIGURATIONS/rho0.02/trj\_stars.Nstars350.rho0.02.lammpstrj*
>>
>>2.2) To visualize wrap coordinates: in the VMD menu select: *Extensions/TK Console* and type in the console *pbc wrap -all*
>>
>>2.3) In the VMD screen move to frame 1 (corresponding to timestep 100000).
>>
>>2.4) In the VMD menu select: *Graphics/Representations/DrawingMethod/VDW*. Then decrease sphere scale to 0.4.
>>
>>2.5) *CreateRep*:
>>
>>serial 270 264 265 652 651 660 654 655 2588 2587 2590 2581 2582 2268 2267 2270 2261 2262 1245 1244 1250 1241 1242 342 341 350 344 345 262 261. These are the particleIDS of the first minimum loop (5). Linked to this, there is a second minimum loop (14). To visualize it we have to:
>>*CreateRep*:
>>
>>serial 200 191 192 872 871 880 877 878 2865 2864 2870 2867 2868 1812 1811 1820 1817 1818 2388 2387 2390 2381 2382 1475 1474 1480 1477 1478 1172 1171 1180 1174 1175 1095 1094 1100 1091 1092 1328 1327 1330 1321 1322 3098 3097 3100 3091 3092 198 197.
>>
>>The particleIDs of all pairs of minimum loops that are linked can be found in the file *LinkedIDs\_100000.dat*.