#include<vector>     /*Library for using vector insted of arrays*/
#include<iostream>   /*Basic functions*/   
#include <cmath>     /*Define some fundamental mathematical operations*/
#include<stdlib.h>   /*Useful for random numbers*/
#include <sstream>
#include <fstream>   /*To read and write into files*/
#include <limits>
#include <stdio.h>   /*Basic functions in particular "sprintf"*/
#include <iomanip>   // std::setprecision
#include <algorithm> //max element


using namespace std;

#define pi 3.141592653589793238462643383279502884197169399375
#define SMALL 1e-10

//Define the maximum number of minloops we will use
const int Nmax=10000;

//Define the maximum number of beads in each minimum-loop. 
//Remember that the Mathematica script found minimum-loops made by at the most 20 DNAns.
//This sets 20*7=140.
const int Nbeadsmax=140;

double gaussLnkNumb(int n1, int n2, double c1[][3], double c2[][3]);


int main(int argc, char *argv[])
{
    /**********************************************/
    /*Pass the following arguments to the programm*/
    /**********************************************/
    /*To get help screen of the program: ./trajectories help*/
    if(argc<8 || argc>8)
    {
        cout << "Four arguments are required by the programm:"  << endl;
        cout << "1.- Path to the input data"                    << endl;
        cout << "2.- Output path"                               << endl;
        cout << "3.- Input file (without timestep)"             << endl;
        cout << "4.- Number of DNAns"                           << endl;
        cout << "5.- Number of particles per DNAns"             << endl;
        cout << "6.- Timestep"                                  << endl;
        cout << "7.- Select a timestep to print the reconstructed linked rings" << endl;
    }


if(argc==8)
{   
    //cout << "ok" << endl;
    /*argv[1] Directory where the data is*/
    /*argv[2] Directory where the output will be stored*/
    /*argv[3] Input file*/
    
    /*Number of DNAns*/
    int M;
    M = atoi(argv[4]);
    /*Number of beads per DNAns*/
    int N;
    N = atoi(argv[5]);
    //cout << "ok" << endl;
    int timestep;
    timestep = atoi(argv[6]);
    
    int td;
    td = atoi(argv[7]);    
    
    //File contains: (1) timestep, (2) l; (3)-(3+l-1)Bead-id
    int l;
    long int id;
    
    //Vector containing the number of beads in each of the minloops
    vector<int> mlSize;
    //The number of minimum loops
    int Nminloops;
    
    //Matrix containing the ids in each loop
    vector< vector<int> > mlIDs(Nmax, vector<int>(Nbeadsmax));
    
    //Wrapped position of the particles in each loop
    vector< vector< vector<double> > > positionOriginal(Nmax, vector<vector<double> >(Nbeadsmax, vector<double>(3)));
    //Reconstructed positions through the PBC
    vector< vector< vector<double> > > position(Nmax, vector<vector<double> >(Nbeadsmax, vector<double>(3)));
    
    int LinkingMap;
    //Print Full linking matrix
    stringstream writeFile3;
    writeFile3 <<"LinkingMatrix.dat";
    ofstream write3(writeFile3.str().c_str(), std::ios_base::app);

    /**************************************************************/
    /**Reads input file with the IDS of beads in the minimum loop**/
    /**************************************************************/
    char readFile[100] = {'\0'};
    sprintf(readFile, "dataMinloop/MinLoopsParticleIDs_t%d.dat",timestep);
    ifstream read(readFile);
    
    //Loop over minloops
    int ml=0;
 
    if(!read){cout << "Error while opening data file!"<<endl;}
    if (read.is_open())    
    {                   
        string line;
        string dummy;                

        //Loop to read the file, line by line into a string.
        while (getline(read, line))            
        {                            
            //To split the line into its corresponding values
            istringstream is(line);
            
            //Reads the number of beads forming the minimum loop
            is >> l;
            mlSize.push_back(l);
            
            //Reads the ParticleID of beads in the minimum loop
            for(int j=0; j<l; j++){
               is >> id;
               mlIDs[ml][j]=id;
            }            
            ml+=1;
        }
    }
    read.close();
    
    //The total number of minimum loops.
    Nminloops=mlSize.size();
    cout << "Nminloops=" << Nminloops << endl;
    
    //The number of beads in the largest minimum loop
    int Nbmax=*max_element(mlSize.begin(), mlSize.end());
    cout << "Nbmax=" << Nbmax << endl;
    
    
    
    /********************************************************/
    /**Reads input file with the positions of all particles**/
    /********************************************************/
    ifstream indata1;
    char readFile1[400] = {'\0'};
    sprintf(readFile1, "%s%s%d",argv[1],argv[3],timestep);        
    indata1.open(readFile1);
    
    //The total number of beads in the system. It should be equal to M*N.
    //With M the number of DNAns and N=10 the number of particles per DNAns
    long int Ntot;        
    long int type,mol,time;
    double   x,y,z;
    int ix,iy,iz;
    double lmin,lmax,Lx,Ly,Lz;
    double comx1, comy1, comz1;
    double comx2, comy2, comz2;
    
    vector<vector<double> > AllPositons(M*N, vector<double>(3));
    
    if (indata1.is_open())
    {       
        string line;
        string dummy;
        
        //Read headers
        for(int i=0;i<10;i++){
            if(i==1) {
                indata1 >> time;
            }

            if(i==3) {
                indata1 >> Ntot;
            }

            if(i==5) {
                indata1 >> lmin >> lmax;
                Lx = lmax-lmin;
            }

            if(i==6) {
                indata1 >> lmin >> lmax;
                Ly = lmax-lmin;
            }

            if(i==7) {
                indata1 >> lmin >> lmax;
                Lz = lmax-lmin;
            }

            else getline(indata1,dummy);
        }
                
        //READ ATOMS
        for(int n=0; n<Ntot; n++){
            indata1 >> id >> type >> x >> y >> z >> ix >> iy >> iz;
            
            AllPositons[id-1][0] = x;
            AllPositons[id-1][1] = y;
            AllPositons[id-1][2] = z;
        }
            
        indata1.close();
        indata1.clear();
    }
   
    else {cout << "Error opening file";}
    
    
    /****************************************************/
    /**Pass the positions of particles in minloops only**/
    /****************************************************/
    for(int m=0;m<Nminloops;m++){
        //NUmber of beads in the minloop m:
        l=mlSize[m];
        
        for(int n=0; n<l;n++){
            //IDs of beads in the minimum loop
            id=mlIDs[m][n];
            x=AllPositons[id-1][0];
            y=AllPositons[id-1][1];
            z=AllPositons[id-1][2];
                        
            //This is static --> will never change
            positionOriginal[m][n][0]=x;
            positionOriginal[m][n][1]=y;
            positionOriginal[m][n][2]=z;
        }
    }
    
    
    ///////////////////////////////////////////////
    //COMPUTE LINKING NUMBER BETWEEN PAIR OF LOOPS/
    ///////////////////////////////////////////////
    double TotalLinking=0;
    double lk=0;
    double Curve1[Nbeadsmax][3];
    double Curve2[Nbeadsmax][3];
    
    for(int m1=0;m1<Nminloops-1;m1++){
        //////////////////////////////
        //Set the first minimum loop//
        //////////////////////////////
        //Number (s1) of beads in the first minimum loop (m1)
        int s1=mlSize[m1];
        
        //Set the position vector to the original position
        for(int n=0; n<s1;n++){
            position[m1][n][0]=positionOriginal[m1][n][0];
            position[m1][n][1]=positionOriginal[m1][n][1];
            position[m1][n][2]=positionOriginal[m1][n][2];
        }
        
        //Reconstruct the first minimum loop through PBC//
        //Maximum distance between consecutive beads in a minimum loop
        double rc=1.25;
        
        for(int n=0; n<s1-1;n++){
            double dx = position[m1][n+1][0] - position[m1][n][0];
            double dy = position[m1][n+1][1] - position[m1][n][1];
            double dz = position[m1][n+1][2] - position[m1][n][2];
            
            double dist=sqrt(dx*dx + dy*dy + dz*dz);
            
            if(dist>rc){
                double fx=round(dx/Lx);
                double fy=round(dy/Ly);
                double fz=round(dz/Lz);

                /*Minimum Image criterion*/
                if (dx >= -0.5*Lx && dx <= 0.5*Lx){ dx += 0.0;}
                if (dy >= -0.5*Ly && dy <= 0.5*Ly){ dy += 0.0;}
                if (dz >= -0.5*Lz && dz <= 0.5*Lz){ dz += 0.0;}

                if (dx > 0.5*Lx){ dx -= abs(fx)*Lx;}
                if (dy > 0.5*Ly){ dy -= abs(fy)*Ly;}
                if (dz > 0.5*Lz){ dz -= abs(fz)*Lz;}

                if (dx < -0.5*Lx){ dx += abs(fx)*Lx;}
                if (dy < -0.5*Ly){ dy += abs(fy)*Ly;}
                if (dz < -0.5*Lz){ dz += abs(fz)*Lz;}

                //change the position of the n+1 bead
               	position[m1][n+1][0] = position[m1][n][0]+dx;
                position[m1][n+1][1] = position[m1][n][1]+dy;
               	position[m1][n+1][2] = position[m1][n][2]+dz;
            }
        }
                
        //Compute the COM of the first loop (m1)
        comx1=0.0;
        comy1=0.0;
        comz1=0.0;
        for(int n=0; n<s1;n++){
            comx1+=position[m1][n][0];
            comy1+=position[m1][n][1];
            comz1+=position[m1][n][2];
        }
        comx1/=(double)s1;
        comy1/=(double)s1;
        comz1/=(double)s1;
        
        //Translate the COM of the first loop so it is in the middle of the box
        for(int n=0; n<s1;n++){
            position[m1][n][0] -= comx1;
            position[m1][n][1] -= comy1;
            position[m1][n][2] -= comz1;
        }
                                
        //Pass the curve representing the first minimum loop
        for(int nb=0;nb<s1;nb++){
            Curve1[nb][0]=position[m1][nb][0];
            Curve1[nb][1]=position[m1][nb][1];
            Curve1[nb][2]=position[m1][nb][2];
        }
        
        //Vector to store the ringID of all the minimum lopps (m2) linked with the first minimum loop (m1)
        vector <int> linkedRings;
        
        ///////////////////////////////////////
        //now look at all other minimum loops//
        ///////////////////////////////////////
        for(int m2=m1+1;m2<Nminloops;m2++){
            int s2=mlSize[m2];
            
            LinkingMap=0;

            //Set the position vector to the original position
            for(int n1=0; n1<s2;n1++){
                position[m2][n1][0]=positionOriginal[m2][n1][0];
                position[m2][n1][1]=positionOriginal[m2][n1][1];
                position[m2][n1][2]=positionOriginal[m2][n1][2];
            }
            
            //Reconstruct second loop through PBC
            for(int n1=0; n1<s2-1;n1++){
                double dx = position[m2][n1+1][0] - position[m2][n1][0];
                double dy = position[m2][n1+1][1] - position[m2][n1][1];
                double dz = position[m2][n1+1][2] - position[m2][n1][2];
            
                double dist=sqrt(dx*dx + dy*dy + dz*dz);
            
                if(dist>rc){
                    double fx=round(dx/Lx);
                    double fy=round(dy/Ly);
                    double fz=round(dz/Lz);

                    /*Minimum Image criterion*/
                    if (dx >= -0.5*Lx && dx <= 0.5*Lx){ dx += 0.0;}
                    if (dy >= -0.5*Ly && dy <= 0.5*Ly){ dy += 0.0;}
                    if (dz >= -0.5*Lz && dz <= 0.5*Lz){ dz += 0.0;}

                    if (dx > 0.5*Lx){ dx -= abs(fx)*Lx;}
                    if (dy > 0.5*Ly){ dy -= abs(fy)*Ly;}
                    if (dz > 0.5*Lz){ dz -= abs(fz)*Lz;}

                    if (dx < -0.5*Lx){ dx += abs(fx)*Lx;}
                    if (dy < -0.5*Ly){ dy += abs(fy)*Ly;}
                    if (dz < -0.5*Lz){ dz += abs(fz)*Lz;}

               	    //change the position of the n+1 bead
               	    position[m2][n1+1][0] = position[m2][n1][0]+dx;
               	    position[m2][n1+1][1] = position[m2][n1][1]+dy;
               	    position[m2][n1+1][2] = position[m2][n1][2]+dz;
                }
            }      

            //Translate the second loop by the same translation that the firs loop COM
            for(int n1=0; n1<s2;n1++){
                position[m2][n1][0]-=comx1;
                position[m2][n1][1]-=comy1;
                position[m2][n1][2]-=comz1;
            }
            
            //Compute the COM of the second loop (m2)
            comx2=0.0;
            comy2=0.0;
            comz2=0.0;
            for(int n1=0; n1<s2;n1++){
                comx2+=position[m2][n1][0];
                comy2+=position[m2][n1][1];
                comz2+=position[m2][n1][2];
            }
            comx2/=(double)s2;
            comy2/=(double)s2;
            comz2/=(double)s2;
                        
            //WRAP COORDINATES of the COM of the second ring            
            double tx=0.0;
            double ty=0.0;
            double tz=0.0;
            
            if (comx2 < -0.5*Lx)  {double comx3 = comx2 + Lx; tx=comx3-comx2;}
            if (comx2 >= 0.5*Lx)  {double comx3 = comx2 - Lx; tx=comx3-comx2;}
            
            if (comy2 < -0.5*Ly)  {double comy3 = comy2 + Ly; ty=comy3-comy2;}
            if (comy2 >= 0.5*Ly)  {double comy3 = comy2 - Ly; ty=comy3-comy2;}
                
            if (comz2 < -0.5*Lz)  {double comz3 = comz2 + Lz; tz=comz3-comz2;}
            if (comz2 >= 0.5*Lz)  {double comz3 = comz2 - Lz; tz=comz3-comz2;}

            //Apply a translation of the whole ring from comx2 to comx3
            for(int n1=0; n1<s2;n1++){
                position[m2][n1][0] += tx;
                position[m2][n1][1] += ty;
                position[m2][n1][2] += tz;
            }
            
            //Pass the curve representing the second minimum loop after wrapping the coordinates
            for(int mb=0;mb<s2;mb++){
                Curve2[mb][0]=position[m2][mb][0];
                Curve2[mb][1]=position[m2][mb][1];
                Curve2[mb][2]=position[m2][mb][2];
       	    }
                
            ////////////////////////////////////
            //Compute Lk between pair of curves/
            ////////////////////////////////////
            //lk=0 when not linked;
            //lk=-nan when the two rings share some beads
            if(m1!=m2){
                lk=gaussLnkNumb(s1,s2,Curve1,Curve2);
                
                //If lk=nan set lk to zero
                 if (lk != lk){lk=0;}
            }
                                                                
            if(lk!=0){
                TotalLinking+=abs(lk);
                LinkingMap=lk;
                write3 << timestep << " " << m1 << " " << m2 << " " << LinkingMap << endl;
                    
                //Store the ringID of all the m2-rings linked with the m1-ring.
                linkedRings.push_back(m2);
                    
                if(timestep==td){
                    //Write the reconstructed configurations of the two rings that are linked
                    stringstream writeFile1;
                    writeFile1 <<"linkedConfigurations/LinkedConfigs_"<< timestep << "_ring1-" << m1+1 << "_ring2-" << m2+1 << ".xyz";
                    ofstream write1(writeFile1.str().c_str());
                    write1 << s1+s2<< endl;
                    write1 << "Atoms. Timestep : 0"<< endl;
                    for(int nb=0;nb<s1;nb++){
                        write1 << "C "  << Curve1[nb][0] << " " << Curve1[nb][1] << " " << Curve1[nb][2] <<endl;
                    }
                    for(int mb=0;mb<s2;mb++){
                        write1 << "O "  << Curve2[mb][0] << " " << Curve2[mb][1] << " " << Curve2[mb][2] <<endl;
                    }
                    write1.close();
                    write1.clear();
                
                    //Write the pariticleIDS in the two minimum loops that are linked
                    stringstream writeFile2;
                    writeFile2 <<"linkedIDS/LinkedIDs_" << timestep <<  ".dat";                
                    ofstream write2(writeFile2.str().c_str(), std::ios_base::app);
                
                    write2 << "ring1=" << m1+1 << ", ring2=" << m2+1 << endl;
                    for(int nb=0;nb<s1;nb++){
                        if(nb==0){write2 << mlIDs[m1][nb];}
                        if(nb>0) {write2 << " " << mlIDs[m1][nb];}
                    }
                    write2 << endl;
            
                    for(int mb=0;mb<s2;mb++){
                        if(mb==0){write2 <<  mlIDs[m2][mb];}
                        if(mb>0) {write2 << " " << mlIDs[m2][mb];}
                    }
                    write2 << endl;
                    write2 << endl;
                }//closes if to write files at timestep td                  
            }//Closes if lk!=0
        }//close loop on m2. Until here we found all the m2-rings that are linked to the m1-ring
                
    }//close loop on m1
    
    cout << "time t= " << timestep << " -> Nlinking = " <<  TotalLinking << endl;
    
    
    //Print results at a given time
    stringstream writeFile4;
    writeFile4 << "Linking_vs_time.dat";
    ofstream write4(writeFile4.str().c_str(), std::ios_base::app);
    write4 << timestep << " " << Nminloops << " " << TotalLinking << endl;
     

}


return 0;
}








double gaussLnkNumb(int n1, int n2, double c1[][3], double c2[][3]){
    double lk=0;
    long double num=0;
    double dr1[3],dr2[3],r[3],cross[3];
    long double dist, dist3;
    for(int n=0 ;n<n1; n++){
        //Flag to exit the loop
        int flag1=0;
        for(int m=0;m<n2;m++){
            r[0] = c2[m][0] - c1[n][0];
            r[1] = c2[m][1] - c1[n][1];
            r[2] = c2[m][2] - c1[n][2];
            dist = sqrt(r[0]*r[0] + r[1]*r[1] + r[2]*r[2]);
            dist3 = dist*dist*dist;
            
            if (dist>0 && dist<SMALL){
                //cout<<"distance between beads in lk calculation is too short "<< dist << endl;
                dist=0.0;
            }                         
            
            if(dist>0){      
    	           r[0]=-r[0];r[1]=-r[1];r[2]=-r[2];

                dr1[0] = c1[(n+1)%n1][0] - c1[n][0];
                dr1[1] = c1[(n+1)%n1][1] - c1[n][1];
                dr1[2] = c1[(n+1)%n1][2] - c1[n][2];
       
                dr2[0] = c2[(m+1)%n2][0] - c2[m][0];
                dr2[1] = c2[(m+1)%n2][1] - c2[m][1];
                dr2[2] = c2[(m+1)%n2][2] - c2[m][2];
       
                cross[0] = dr1[1]*dr2[2] - dr1[2]*dr2[1];
                cross[1] = dr1[2]*dr2[0] - dr1[0]*dr2[2];
                cross[2] = dr1[0]*dr2[1] - dr1[1]*dr2[0];
       
                num = r[0]*cross[0] + r[1]*cross[1] + r[2]*cross[2];
       
                lk += num*1.0/dist3;
            }
        
            //This happens when two minimum loops share some beads    
            if(dist==0){lk=0; flag1=1; break;}
        }
        
        //Exit the calculation because minimum loops share beads
        if(flag1==1){break;}
    }
    return round(lk*1.0/(4.0*M_PI));
}
