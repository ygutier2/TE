# Minimum loops analysis

The bash script (**bash_fractalDimMinLoops.sh**) contains the instructions to perform the analysis related to identifying minimum loops of DNAns (not linking).

## I.- The mathematica notebook
The mathematica script **MinLoops.m** reads the data from the network connectivity files in folder **1_networkConnectivity/REP1/**. These files contain information about which DNAns (with  moleculeID $m\in[0,N-1]$) is connected with any other DNAns. Then,  the mathematica script looks for the minimum loops containing molecule $r_{1}$ and formed by $l_{min} \in [0,l_{m}]$ nanostars, with $l_{m}=20$ a cutoff size for the loops found by our analysis. This is repeated for every single vertex ($r_{1}$) in the graph. For instance, if the minimum loop passing through $r_{1}=0$ is made by $l_{min}=6$ nanostars (0,1,5,27,38 and 100). This means that when the algoritm looks for the minimum loop passing through $r_{1}=1$, there is the possibility that the minimum loop would contain the same nanostars (1,5,27,38,100 and 0). In this case, the number of minimum loops would be over counted, so later we would ensure that this is not the case.

By default the analysis is performed for the first two frames from simulations ($t=0$ and $t=100000$), but the user can change this by setting the initial ($t_{i}$), final ($t_{f}$) and dump frequency ($d_{freq}$) times in the notebook. The analysis will create the file **minLoop.txt** with columns:

> 1.- timestep
>
> 2.- The number of DNAns ($l_{min}$) connected in the minimum loop that contains molecule r1.
>
> 3.- The next columns represent the molecule-id(minus 1) of all DNAns connected (in order and starting from $r_{1}$).

Also the file *attempts.txt* will be created, with columns:
> 1.- Timestep
>
> 2.- The number of successful attempts to find a minimum loop.
>
> 3.- The number of unsuccessful attempts to find a minimum loop (because the chosen vertex was not part of a loop made by at the most 20 DNAns).

**Note:** if you have problems running the mathematica script through the bash, you will need to open the script (MinLoops.m) and run it as usual (shift+enter) to create the previous files. Then continue running bash\_fractalDimMinLoops.sh.

## II.- Fractal dimension
Runs the AWK script **awk.remove_repeated_loops.sh** which creates the file *minLoop_cleaned.txt*. This is a copy of *minLoop.txt* after removing repeated minimum loops.

Runs the C++ code **fractalD_ring_v2.cpp** which reads the moleculeIDs from *minLoop_cleaned.txt* and identifies the 3D configuration of this path in our simulations (after reconstruction of the path using the minimum image criterion). It computes the radius of gyration ($R_{g,min}$). To avoid considering linear paths closed through the periodic boundary conditions as a minimum loop, we imposed the condition $R_{g,min}$<= 0.3L $=12\sigma$. This analysis creates the file **fractalDimension2.dat**, with $3+l_{min}$ columns:
> 1.- Timestep.
>
> 2.- The number of DNAns ($l_{min}$) in the minimum loop.
>
> 3.- The radius of gyration of the minimum loop.
>
> From column 4 onwards: the molecule-id(minus 1) of the nanostars forming the minimum loop.

Runs the AWK script (**awk.histbin**), which computes the probability distribution $P(l_{min})$ of observing in the simulation minimum loops made by $l_{min}$ DNAns. This informations is dumped to the file **distprob**.

Using AWK to analyse the data in *fractalDimension2.dat*, it creates the file **avelmin_Rgmin.dat** with four columns:

> 1.- <$l_{min}$>.
>
> 2.- Standard deviation of $l_{min}$.
>
> 3.- <$R_{g,l_{min}}$>
>
> 4.- Standard deviation of $R_{g,l_{min}}$.

Runs the AWK script **awk.Nminloop_vs_time.sh** to compute the total number of unrepeted minimum loops as function of time (**NminLoops.txt**) and also the average over time and standard deviation of the number of minimum loops (**aveNmin.dat**).

Runs the AWK script in **awk.ave_at_same_l.sh** which creates the file **average_l_R_Rg.dat** with the following columns:.
> 1.- $l_{min}$
>
> 2.- <$R_{g,min}$>, where the average is taken over all minimum loops with the same $l_{min}$
>
> 3.- Standard deviation of the radius of gyration.

## Linking number
Please check the README file inside the folder **Linking** for further details and the codes to compute the linking number.
