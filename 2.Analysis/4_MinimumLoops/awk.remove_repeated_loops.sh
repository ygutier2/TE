#!/bin/bash

data_file="minLoop.txt"

############################################################################
## Sort each row (horizontally) in ascending order and store it into a file#
############################################################################
echo "Sorting line"
fsorted="${data_file}_sorted"
awk '{
    # Initialize the arrays
    numCols = NF
    for (i = 1; i <= numCols; i++) {
        array[i] = $i
    }

    # Sort the array starting from the third component onwards (moleculeIDs forming the loop)
    for (i = 3; i <= numCols; i++) {
        for (j = i + 1; j <= numCols; j++) {
            if (array[i] > array[j]) {
                temp = array[i]
                array[i] = array[j]
                array[j] = temp
            }
        }
    }        

    # Print the result
    for( i = 1; i <= numCols; i++ ){
        printf( "%s ", array[i] ); 
    }
    printf("\n")
    
}' "$data_file" > ${fsorted}


###########################################
## Find the line numbers that are repeated#
###########################################
echo "Finding line number of repeated lines"
frep="${data_file}_replines"
awk '{
    if (seen[$0]++) {
        repeated_lines[$0] = (repeated_lines[$0] == "" ? "" : repeated_lines[$0] RS) NR
    }
}

END {
    for (line in repeated_lines) {
        print repeated_lines[line]
    }
}' ${fsorted} > ${frep}


##########################################
## remove repeated lines from minLoop.txt#
##########################################
echo "Removing repeated lines"

#If the file ${frep} IS NOT empty, then delay repeated lines and create Loops_cleaned.txt
if [ -s ${frep} ]; then
    awk 'NR==FNR{a[$0]=1;next}!a[FNR]' ${frep} ${data_file} > temp.txt
#If the file ${frep} IS empty, then copy sorted contain into minLoop_cleaned.txt
else
    cp ${fsorted} temp.txt
fi

#############
## Sort file#
#############

sort -k1,1n -k2,2n temp.txt > minLoop_cleaned.txt


rm ${frep}
rm ${fsorted}
rm temp.txt
