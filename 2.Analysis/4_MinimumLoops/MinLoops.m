(* ::Package:: *)

(* ::Input::Initialization:: *)
(*Uncomment next line if running from Graphical Interface*)

(*SetDirectory[NotebookDirectory[]];*)

(***********)
(*Variables*)
(***********)
(*Number of DNAns*)
M=350;
(*Initial time, dump frequency and last timestep*)
ti=0;
dfreq=100000;
tf=100000;
frames=(tf-ti)/dfreq+1;
(*The minimum and maximum number of DNAns forming a loop*)
Nmin=4;
Nmax=20;


(************************************************)
(*Analyse data from different snapshots in a loop*)
(************************************************)
For[t=0,t<frames,t++,
timestep=ti+t*dfreq;
data=Import["../1_networkConnectivity/REP1/network_connectivity_t"<>ToString[timestep]<>"_Nstars"<>ToString[M]<>"_rho_0.02_REP1.dat","Table"];

(*Molecules (stars) that are connected*)
datamol=Table[data[[i]][[1]]\[UndirectedEdge]data[[i]][[2]],{i,1,Length[data]}];
gmol=Graph[datamol,VertexLabels->None];

(*Create a list with all the vertices included in the graph gmol*)
(*This is done because, if a DNAns was not connected with any other DNAns*)
(*then, it was not included in the graph*)
vl=VertexList[gmol];

(*Compute the minimum loop passing through each of the DNAns.*)
n=0;
(*Successfull and unsuccessful attempts to compute a loop*)
nsuccess=0;
nfail=0;
Nloops=0;
While[n<M,
r1=n;
n++;
(*Check if r1 is a vertex in the graph*)
flag=0;
existr1=MemberQ[vl,r1];
If[existr1,flag++,nfail++;Continue[]];
(*If vertex exists*)
If[flag==1,
(*Check if a loop contains vertex r1*)
flag2=0;
(*Find all cycles containing vertex r1 and formed by ll particles (at the most ll=Nmax particles)*)
For[ll=Nmin,ll<=Nmax,ll++,
fc=FindCycle[{gmol,r1},ll,All];
(*Number of loops found*)
l=Length[fc];
If[l>0,
flag2++;
(*Compute the number of vertices in each of the loops*)
ml=Map[Length,fc];
(*Minimum of the previous array: Number of DNAns in the minimum loop*)
min=Min[ml];
(*Compute the graph (connection between vertices) in that minimum loop*)
fcmin=FindCycle[{gmol,r1},{min}];
(*Remove the outter most brackets from the previous result*)
flatfcmin=Flatten[fcmin,1];
(*Obtain a list of the vertices that are part of the minimum loop*)
verlist=VertexList[flatfcmin];
PutAppend[StringRiffle[{timestep,min,verlist}],"minLoop.txt"];
(*Since you have found loops, stop the calculation. No need to increase ll*)
Break[];
,
(*If l is not larger than 0, continue to the next ll*)
Continue[];
];(*Close if statement on l*)
];(*Close for loop on the size ll of loops*)
If[flag2==0,nfail++,nsuccess++];
,
(*If flag1!=1, meaning that r1 vertex is not part of the graph, then continue to next vertex n++*)
Continue[];
];(*Close if statement on flag*)
];(*Close while loop on attempts*)
PutAppend[StringRiffle[{timestep,nsuccess,nfail}],"attempts.txt"];
];(*Close for loop on time*)
