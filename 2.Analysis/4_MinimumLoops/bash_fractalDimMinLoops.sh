#!/bin/bash
#######################################################
## System details: needs to be filled-in by the user ##
#######################################################
  Nstars=350
  Nparticlesperstar=10
  rho=0.02
  
  #Current working directory
  cwd=$(pwd)
  


###########################
#Run the mathematica script
###########################
#Note that if you don't have X11 installed you will need to open the script (double click to network_connectivity.m) and run it as usual (shift+enter).
math -script MinLoops.m

############
#Data format
############
#Remove new lines found after the special character \
sed -i '/\\$/{N;s/\n//}' minLoop.txt

#Remove the \ special characters
sed -i 's/\\//g' minLoop.txt

#Remove all the ",{} characters
sed -i 's/"//g' minLoop.txt
sed -i 's/{//g' minLoop.txt
sed -i 's/}//g' minLoop.txt
sed -i 's/,//g' minLoop.txt

#Remove " from attempts file
sed -i 's/"//g' attempts.txt

##################################################################
#Use AWK script to remove repeated minimum loops at a fixed time#
##################################################################
#This program will create the file "minLoop_cleaned.txt" is a copy of the original files but we have removed the repeated minimum loops
echo "REMOVING REPEATED LOOPS"
./awk.remove_repeated_loops.sh

#######################################################################################################################################
#Compute the Rg of the minLoop. Then, choose the DNAns with Rg<12 sigma and the ones with Re2e<rc (where rc is the diameter of a DNAns)
#######################################################################################################################################
echo "COMPUTING Rg"
 #1.- Path to the input data
 datapath="../CONFIGURATIONS/rho${rho}/"
      
 #2.- Output path
 outputpath="./"
      
 #3.- Input file with atoms information (without timestep)
 rfile="stars.Nstars${Nstars}.rho${rho}."
 
 c++ fractalD_ring_v2.cpp -o fractalD_ring_v2.out -lm
 
 # READ Variables from the file created by mathematica #
 #######################################################
 awk -v dp="${datapath}" -v op="${outputpath}" -v rf="${rfile}" '{
   arguments = dp" "op" "rf;
   
   for (i = 1; i <= NF; i++) {
     col[i] = $i;
     arguments = arguments" "col[i];     
   }
   
   #print "Arguments:", arguments;
   # Execute the C++ program and pass the concatenated values as a single argument
   cmd = "./fractalD_ring_v2.out " arguments;
   system(cmd);
 }' minLoop_cleaned.txt
 

 rm fractalD_ring_v2.out 

 #This programm will create a file called (fractalDimension2.dat) with the following columns:
 #1.-Timestep
 #2.-lmin
 #3.-Rgmin
 #>4.- the molecule-id of the lmin molecules in the loop
      
##################################################
#Compute the histogram of the minimum loops length
##################################################
echo "Computing histogram"
   
# minimum value of x axis
m="0"
  
# maximum value of x axis
M="20"
  
# number of bins
b="20"

# output file for plotting
fout="distprob"
  
# input file
fin="fractalDimension2.dat"
  
gawk -f awk.histbin ${m} ${M} ${b} ${fout} ${fin}



###############################################################################
#Compute the average length and radius of gyration of REAL UNREPEADTED MINLOOPS
###############################################################################
#The avergae is performed over all timesteps
awk '{aveLmin+=$2; aveRgmin+=$3; i++; deltaL=$2-avgL; avgL+=deltaL/NR; meanL2+=deltaL*($2-avgL); deltaR=$3-avgR; avgR+=deltaR/NR; meanR2+=deltaR*($3-avgR);} END { print aveLmin/i,sqrt(meanL2/NR), aveRgmin/i,sqrt(meanR2/NR); }' fractalDimension2.dat > avelmin_Rgmin.dat



####################################################################
#Compute the number of REAL UNREPEADTED MINLOOPS as function of time
####################################################################
 echo "Computing Nloops"
 chmod a+x awk.Nminloop_vs_time.sh
 ./awk.Nminloop_vs_time.sh
 #This program will create "NminLoops.txt" with the following columns:
 #1.- timestep
 #2.- Total number of unrepeated minimum loops (regardless their size).

 #Also compute the average over time of the number of minimum loops
 awk '{aveNmin+=$2; i++; deltaN=$2-avgN; avgN+=deltaN/NR; meanN2+=deltaN*($2-avgN);} END { print aveNmin/i,sqrt(meanN2/NR); }' NminLoops.txt > aveNmin.dat



################################################################
#Compute average of R and Rg at the same l from the scatter plot
################################################################
 echo "Average Rg at same l"
 chmod a+x awk.ave_at_same_lmin.sh
 ./awk.ave_at_same_lmin.sh

 echo "Finish of the analysis"
