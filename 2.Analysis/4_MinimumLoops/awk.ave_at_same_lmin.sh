#!/bin/bash

data_file="fractalDimension2.dat"

awk '
{    
    sumRg[$2] += $3;         # Accumulate the sum of Rg values (column 3) for each unique value of lmin (column 3)
    sumsqRg[$2] += $3 * $3;  # Accumulate the sum of Rg*Rg
    
    count[$2]++;             # Keep track of the count of values for each unique lambda
}
END {
    for (key in sumRg) {
        averageRg = sumRg[key] / count[key];                           # Calculate the average of Rg
        varianceRg = (sumsqRg[key] - sumRg[key] * sumRg[key] / count[key]) / count[key];  # Calculate the variance of Rg
        stddevRg = sqrt(varianceRg);                                   # Calculate the standard deviation of Rg
        
        print key, averageRg, stddevRg;               # Print lambda, <R>, stdevR, <Rg> and stdevRg
    }
}
' "$data_file" > "average_Rg_atfixed_l.dat"
