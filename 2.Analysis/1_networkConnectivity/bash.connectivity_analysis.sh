#!/bin/bash
  ## The position of the particles are in wrapped coordinates.
  
  #The number of replicas
  nr=1
  
  #Compile the programm:
  c++ -std=c++11 connectivity_vs_t.cpp -o connectivity_vs_t
  
  #Current working directory
  cwd=$(pwd)

####################################################
## This section needs to be filled-in by the user ##
####################################################
  #System details
  Nstars=350
  Nparticlesperstar=10
  rho="0.02"
  
  #Distance cut-off to consider a contact between the patches of nanostars
  rc="0.2"
  
  #Start the calculation from this timestep
  ts=0
  #Dump frequency
  dfreq=100000
  #Last timestep
  tl=1000000 
  
 
  
  
##############################################
## Execute the c++ script connectivity_vs_t ##
##############################################
  for i in $( seq 1 ${nr} )
  do
      #1.- Path to the input data
      datapath="../CONFIGURATIONS/rho${rho}/"
      
      #2.- Output path
      outputpath="./"
      
      #3.- Input file with atoms information (without timestep)
      rfile="stars.Nstars${Nstars}.rho${rho}."
      
      #The total number of files inside the DUMP folder (for each replica)
      nf=$(ls -lorth ${datapath}${rfile}* | wc -l)
      echo "The total number of data files in replica $i is:  ${nf}"
    
      if [ "$nf" -gt 0 ]
      then
          for timestep in $( seq ${ts} ${dfreq} ${tl} )
          do
              #1.- Path to the input data
              #2.- Output path
              #3.- Input file with atoms information (without timestep)
              #4.- timestep
              #5.- Number of molecules
              #6.- Number of beads per molecule
              #7.- Cutoff distance for contact
              
              ./connectivity_vs_t ${datapath} ${outputpath} ${rfile} ${timestep} ${Nstars} ${Nparticlesperstar} ${rc}
              
              #At each timestep a file is generated with the network connectivity information
              mv network_connectivity_t${timestep}.dat network_connectivity_t${timestep}_Nstars${Nstars}_rho_${rho}_REP${i}.dat
          done
          
          #This output file contains the Nc as funtion of the Brownian time
          mv nc_vs_t.dat nc_vs_t_Nstars${Nstars}_rho_${rho}_REP${i}.dat
      fi
  done
  
  rm connectivity_vs_t
    
  #Create a directory and move the network connectivity information for each replica
  for i in $( seq 1 ${nr} )
  do
      mkdir REP${i}
      mv network_connectivity_t*_REP${i}.dat REP${i}/
  done
  
####################################################
## Execute the mathematica script in the terminal ##
####################################################
  #Note that if you don't have X11 installed you will need to open the script (double click to network_connectivity.m) and run it as usual (shift+enter).
  #ALso note that this is not so important for the analysis but just to visualize the graphs
  math -script network_connectivity.m
  
  
###############################################################################
#At a fixed timestep check the number of contacts of molecule m, with the rest#
###############################################################################
  Nm1=$((Nstars-1))
  sed -i "s/end =.*/end = ${Nm1}/g" occurrences.awk
  
  for i in $( seq 1 ${nr} )
  do      
     mkdir Valence_REP${i}
  
     for tt in $( seq ${ts} ${dfreq} ${tl} )
     do 
        ./occurrences.awk REP${i}/network_connectivity_t${tt}_Nstars${Nstars}_rho_${rho}_REP${i}.dat >> foccurrences_frame${tt}.txt
    
        #check the frequencies of each valence
        awk '{count[$2]++} END {for (num in count) print num, count[num]}' foccurrences_frame${tt}.txt >> frequency_frame${tt}.txt
    
        mv foccurrences_frame${tt}.txt Valence_REP${i}/
        mv frequency_frame${tt}.txt    Valence_REP${i}/
     done
  done

  
  
  for i in $( seq 1 ${nr} )
  do 
     for tt in $( seq ${ts} ${dfreq} ${tl} )
     do 
        datapath="Valence_REP${i}/"
        file="frequency_frame${tt}.txt"


        #Check that 0 appears in the first column of the file:
        awk '$1 == 0 { found = 1; exit } END { if (!found) print "0 0"; else print $0 }' ${datapath}${file} >> temp1.txt
      
        #Check that 1 appears in the first column of the file:
        awk '$1 == 1 { found = 1; exit } END { if (!found) print "1 0"; else print $0 }' ${datapath}${file} >> temp1.txt
      
        #Check that 2 appears in the first column of the file:
        awk '$1 == 2 { found = 1; exit } END { if (!found) print "2 0"; else print $0 }' ${datapath}${file} >> temp1.txt
      
        #Check that 3 appears in the first column of the file:
        awk '$1 == 3 { found = 1; exit } END { if (!found) print "3 0"; else print $0 }' ${datapath}${file} >> temp1.txt
        
        #Transpose the second column:
        awk '{print $2}' temp1.txt | paste -sd ' ' - > temp2.txt
        
        #append to first column the value of time:
        awk -v r="${tt}" '{print r,$0}' temp2.txt >> valence_vs_time_REP${i}.txt
    
        rm temp1.txt
        rm temp2.txt  
     done
  done