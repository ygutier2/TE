#!/usr/bin/awk -f

# Main AWK script
BEGIN {
    # Set the range of numbers to search for
    start = 0
    end = 349

    # Initialize the count array
    for (i = start; i <= end; i++) {
        count[i] = 0
    }
}

{
    # Increment the count for each number in the first column
    count[$1]++

    # Increment the count for each number in the second column
    count[$2]++
}

END {
    # Print the results for each number in the range
    for (n = start; n <= end; n++) {
        print n, count[n]
    }
}
