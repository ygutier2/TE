#include<vector>
#include<iostream>
#include <cmath>
#include<stdlib.h>
#include <sstream>
#include <fstream>
#include <limits>
#include <stdio.h>
#include <iomanip>
#include <tuple>
#include <set>
#include <algorithm>
#include <map>


using namespace std;

#define pi 3.141592653589793238462643383279502884197169399375
#define SMALL 1e-20

//The timestep set in lammps
double dt=0.01;

int main(int argc, char *argv[])
{
    /**********************************************/
    /*Pass the following arguments to the programm*/
    /**********************************************/
    /*To get help screen of the program: ./trajectories help*/
    if(argc<8)
    {
        cout << "Six arguments are required by the programm:"               << endl;
        cout << "1.- Path to the input data"                                << endl;
        cout << "2.- Output path"                                           << endl;
        cout << "3.- Input file with atoms (without timestep)"              << endl;
        cout << "4.- Timestep"                                              << endl;
        cout << "5.- Number of DNAns"                                       << endl;
        cout << "6.- Nbeads per molecule"                                   << endl;
        cout << "7.- Cutoff distance for a contact"                         << endl;
    }

if(argc==8)
{   
    //cout << "ok" << endl;
    /*argv[1] Directory where the data is*/
    /*argv[2] Directory where the output will be stored*/
    /*argv[3] Input file*/
    
    /*timestep*/
    int timestep;
    timestep = atoi(argv[4]);

    /*Number of polymers in the simulation at t=0*/
    int M;
    M = atoi(argv[5]);
    
    /*Number of beads per polymer*/
    int Nbeads;
    Nbeads = atoi(argv[6]);
    
    /*Number of patches per molecule*/
    int Np=3;
    
    /*Cutoff distance for a contact*/
    float rc;
    rc = atof(argv[7]);
    
      
    vector< vector< vector<double> > > position(M, vector<vector<double> >(Nbeads, vector<double>(3)));
    vector< vector<int> > typevec(M, vector<int>(Nbeads));
    vector< vector<int> > indice(M, vector<int>(Nbeads));
    vector<double> rg2vec(M);
    
    
    /*the following variables will be useful to read the data file*/
    long long int tbrownian;
    long long int time;
    long int Ntot;        //The total number of beads in the system. It should be equal to M*Nbeads.
    long int id,type,mol;
    double   x,y,z;
    int ix,iy,iz;
    double lmin,lmax,Lx,Ly,Lz;    
    int nm;
    
    /*Read the file with ids of terminal beads*/
    ifstream indata1;
    char readFile1[400] = {'\0'};
    string dummy;
    string line;
    sprintf(readFile1, "%s%s%d",argv[1],argv[3],timestep);
    indata1.open(readFile1);
    
    if (indata1.is_open())
    {       
        //Read headers
        for(int i=0;i<10;i++){
            if(i==1) {
                indata1 >> time;                   
                tbrownian = time*dt;
            }

            if(i==3) {indata1 >> Ntot;}

            if(i==5) {
                indata1 >> lmin >> lmax;
                Lx = lmax-lmin;
            }

            if(i==6) {
                indata1 >> lmin >> lmax;
                Ly = lmax-lmin;
            }

                if(i==7) {
                indata1 >> lmin >> lmax;
                Lz = lmax-lmin;
            }

                else getline(indata1,dummy);
        }

        //READ ATOMS
        for(int n=0; n<Ntot; n++){
            indata1 >> id >> type >> x >> y >> z >> ix >> iy >> iz;
            
            int m = floor((double)(id-1)/(1.0*Nbeads));
            
            position[m][(id-1)%Nbeads][0] = x;
            position[m][(id-1)%Nbeads][1] = y;
            position[m][(id-1)%Nbeads][2] = z;
            
            typevec[m][(id-1)%Nbeads] = type;
            indice[m][(id-1)%Nbeads] = id;
        }
        
        indata1.close();        
        indata1.clear();  
    }
   
    else {cout << "Error opening file" << endl;}
 
   
    //Store only position of patches
    vector< vector< vector<double> > > posPatches(M, vector<vector<double> >(Np, vector<double>(3)));
    vector< vector<int> > index(M, vector<int>(Np));

    for(int m=0; m<M; m++){
            int idx=0;
        for(int n=0; n<Nbeads;n++){
            if(typevec[m][n]==2){
                for(int d=0; d<3;d++){
                    posPatches[m][idx][d] = position[m][n][d];
                    index[m][idx]    = indice[m][n];
                }
                idx+=1;
            }
        }
    }
    

    /////////////////////////////
    // COMPUTE Number of contacts
    ////////////////////////////
    vector<int> chequeo;
    //The two pacthes connectig two stars
    vector<int> patch1;
    vector<int> patch2;
    //The two molecules (stars) connected
    vector<int> star1;
    vector<int> star2;
    
    int nc=0;
    double dx,dy,dz, dist;
    for(int m=0;m<M-1;m++){
        for(int n=0; n<Np; n++){
            x=posPatches[m][n][0];
            y=posPatches[m][n][1];
            z=posPatches[m][n][2];
            id=index[m][n];
            
            for(int m1=m+1;m1<M;m1++){
                for(int n1=0; n1<Np; n1++){
                    double x1=posPatches[m1][n1][0];
                    double y1=posPatches[m1][n1][1];
                    double z1=posPatches[m1][n1][2];
                    int   id1=index[m1][n1];
                    
                    dx=x-x1;
                    dy=y-y1;
                    dz=z-z1;
                    
                    dist = sqrt(dx*dx+dy*dy+dz*dz);
                                         
                    if(dist<rc){
                        nc+=1;                             
                        chequeo.push_back(id);
                        chequeo.push_back(id1);
                        
                        //The molecules (stars) that are connected
                        star1.push_back(m);
                        star2.push_back(m1);
                        
                        //The two previous stars are connected through the following patches
                        patch1.push_back(id);
                        patch2.push_back(id1);                  
                    }                    
                }
            }
        }
    }
   
    vector<int> duplicate; 
    // STL function to sort the vector
    sort(chequeo.begin(), chequeo.end());
    
    
    // Create a map to store the frequency of each element
    std::map<int, int> frequencyMap;

    // Count the frequency of each element
    for (int num : chequeo) {
        frequencyMap[num]++;
    }

    // Display duplicates and their frequencies
    int Totalduplicates=0;
    bool foundDuplicates = false;
    for (const auto &entry : frequencyMap) {
        if (entry.second > 1) {
            foundDuplicates = true;
            Totalduplicates+=entry.second-1;
            cout << "Timestep=" << timestep << ",\t Duplicated patchID: " << entry.first << ",\t Frequency: " << entry.second << std::endl;
        }
    }
    cout << "Timestep=" <<timestep << ",\t Total number of duplicated connections: " << Totalduplicates << ",\t out of: " << M*3 << endl;
    cout << endl;
    

    /*
    if (!foundDuplicates) {
        cout << timestep << " No duplicates found." << std::endl;
    }
    */
 
       
    /////////////
    // PRINT NC
    /////////////
    stringstream writeFile1;
    ofstream write1;
    writeFile1 <<"nc_vs_t.dat";
    write1.open(writeFile1.str().c_str(), std::ios_base::app);
    write1 << tbrownian << " " << nc << endl;
    
    //////////////////////////////////////////////
    // PRINT Network connection at each timestep
    //////////////////////////////////////////////
    if(star1.size()>0){
        char writeFile2[400] = {'\0'};
        sprintf(writeFile2, "%snetwork_connectivity_t%d.dat",argv[2],timestep);
        ofstream write2;
        write2.open(writeFile2);
    
        for(int i=0; i<star1.size();i++){
            write2 << star1[i] << " " << star2[i] << " " << patch1[i] << " " << patch2[i] << endl;
        }
        write2.close();
        write2.clear();
    }
             
}


return 0;
}
