(* ::Package:: *)

(* ::Input::Initialization:: *)
(*Uncomment next line if running from Graphical Interface*)

(*SetDirectory[NotebookDirectory[]];*)

data=Import["REP1/network_connectivity_t1000000_Nstars350_rho_0.02_REP1.dat","Table"];
(*Molecules (stars) that are connected*)
datamol=Table[data[[i]][[1]]\[UndirectedEdge]data[[i]][[2]],{i,1,Length[data]}];
gmol=Graph[datamol];
(*Number of DNAns in the largest connected component*)
ConnectedComponents[gmol];
Length[ConnectedComponents[gmol][[1]]]
(*Vertex are highlighted according to their connectivity: 3(red), 2(yellow)*)
HighlightVertexDegree[g_,vd_]:=HighlightGraph[g,Table[Style[VertexList[g][[i]],ColorData["TemperatureMap"][vd[[i]]/Max[vd]]],{i,VertexCount[g]}]];
vd=VertexDegree[gmol];
gmolexport=HighlightVertexDegree[gmol,vd];
UsingFrontEnd[Export["Graph_t1000000.png",gmolexport]];
h1=Histogram[VertexDegree[gmol],Automatic];
UsingFrontEnd[Export["Histogram_t1000000.png",h1]];
(*Patches (ids) that are connected*)
datapatch=Table[data[[i]][[3]]\[UndirectedEdge]data[[i]][[4]],{i,1,Length[data]}];
gpatch=Graph[datapatch];
ConnectedComponents[gpatch];
Length[ConnectedComponents[gpatch][[1]]]
