(* ::Package:: *)

(* ::Input::Initialization:: *)
(*Uncomment next line if running from Graphical Interface*)

(*SetDirectory[NotebookDirectory[]];*)

(*Create directory*)
CreateDirectory["pathLength"];
(***********)
(*Variables*)
(***********)
(*Number of DNAns*)
M=350;
(*Initial time, dump frequency and last timestep*)
ti=0;
dfreq=100000;
tf=100000;
frames=Floor[(tf-ti)/dfreq]+1


(************************************************)
(*Analyse data from different snapshots in a loop*)
(************************************************)
(*Create an empty array that will contain the length of the shortest pathin between all possible branching points from all snapshots*)
fullpathLength={};

For[t=0,t<frames,t++,
timestep=ti+t*dfreq;
data=Import["../1_networkConnectivity/REP1/network_connectivity_t"<>ToString[timestep]<>"_Nstars"<>ToString[M]<>"_rho_0.02_REP1.dat","Table"];

(*Molecules (stars) that are connected*)
datamol=Table[data[[i]][[1]]\[UndirectedEdge]data[[i]][[2]],{i,1,Length[data]}];
gmol=Graph[datamol,VertexLabels->None];

(*Get all the vertex-names that are fully connected*)
nameBranching=Pick[VertexList[gmol],VertexDegree[gmol],3];

(*Number of branching points*)
Nbr=Length[nameBranching];
(*Create empty array for the shortest path length between branching poins at a give timestep*)
pathLength={};
(***********************************************************************************)
(*We want to find the minimum path in between all pairs of fully-connected vertices*)
(***********************************************************************************)
(*Successfull and unsuccessful attempts to find shortest loop*)
nsuccess=0;
nfail=0;
Nattempts=0;
n1=1;
While[n1<Nbr,
r1=nameBranching[[n1]];
n2=n1+1;
While[n2<=Nbr,
Nattempts++;
r2=nameBranching[[n2]];
sp=FindShortestPath[gmol,r1,r2];
l=Length[sp]-1;
(*Count number of failed attempts (when l<0) and successfull attempts (when l>=0)*)
If[l<0,nfail++,nsuccess++;AppendTo[pathLength,l];AppendTo[fullpathLength,l]];
PutAppend[StringRiffle[{timestep,l,sp}],"shortestpath.txt"];
n2++;
];(*Close while on n2*)
n1++;
](*Close while on n1*)
PutAppend[StringRiffle[{timestep,nsuccess,nfail,Nattempts}],"attempts.txt"];
Export["pathLength/PathLength_t"<>ToString[timestep]<>".dat",pathLength];
](*Close while on time*)

(*Export*)
Export["fullPathLength.dat",fullpathLength];
hgram=Histogram[fullpathLength,Automatic];
Export["histogramMathematica.pdf",hgram];
