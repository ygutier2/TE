#!/bin/bash

 #Frequency histogram.
 awk '{counts[$1]++} END {for (c in counts) print c, counts[c]}' fullPathLength.dat | sort -nk1 > histogram.txt
 
 #Normalized histogram
  # minimum value of x axis
  m="0"
  
  # maximum value of x axis
  M="20"
  
  # number of bins
  b="20"

  # output file for plotting
  fout="histogram_distprob"
  
  # input file
  fin="fullPathLength.dat"
  
  gawk -f awk.histLmin ${m} ${M} ${b} ${fout} ${fin}
