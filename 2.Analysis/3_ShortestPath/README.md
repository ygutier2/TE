# Shortest Path between branching points analysis

The bash script (**bash_fractalDimension.sh**) contains the instructions to perform all the analysis in this folder:

## I.- The mathematica notebook
The mathematica notebook **MinPathBranching.m** reads the data from the network connectivity files in folder **1\_networkConnectivity/REP1/**. These files contain information about which DNAns (with  moleculeID $m\in[0,N-1]$) is connected with any other DNAns. Then, it looks for all the fully connected DNAns (with degree 3), these are the branching points. Then, it finds the shortest path in between any two branching points. By default the analysis is performed for the first two frames from simulations ($t=0$ and $t=100000$), but the user can change this by setting the initial ($t_{i}$), final ($t_{f}$) and dump frequency ($d_{freq}$) times in the notebook. The analysis will create the file **shortestpath.txt** with columns:

> 1.- timestep
>
> 2.- The number of DNAns ($\lambda$) that connect the branching point ($r_{1}$) with other branching point ($r_{2}$)
>
> 3.- The next columns represent the molecule-id(minus 1) of all DNAns connected (in order, from $r_1$ until $r_2$).

Also, the file **attempts.txt** will be created, with columns:
> 1.- Timestep
>
> 2.- The number of successful attempts to find at least a path between $r_1$ and $r_2$.
>
> 3.- The number of unsuccessful attempts to find a path between $r_1$ and $r_2$.
>
> 4.- The total number of attempts. This should be equal to nsuccess+nfail.

In addition, the following are created:

* The folder **pathLength** which contains a file per timestep. Each of these files contains the length of the shortest path between all branching points.
* The file **fullPathLength.dat** with the information of the previous point but for all the timesteps.
* The file **histogramMathematica.pdf** with the frequency histogram for $\lambda$.

**Note:** if you have problems running the mathematica script through the bash, you will need to open the script (MinPathBranching.m) and run it as usual (shift+enter) to create the previous files. Then continue running bash\_fractalDimension.sh.

## II.- Histogram of branching points
The bash (**histogram\_freq.sh**) script has the instructions to run an AWK script (**awk.histLmin**), which reads *fullPathLength.dat* and computes:

> *histogram.txt* with the frequency of $\lambda$ between branching points.
>
> *histogram_distprob* with the probability distribution of $P(\lambda)$.

## III.- Fractal dimension
Runs the C++ code **fractalD.cpp**, which reads the moleculeIDs from *shortestpath.txt* and identifies the 3D configuration of this path in our simulations (after reconstruction of the path using the minimum image criterion). Then, it creates the file **fractalDimension.dat**, with six columns

> 1.- timestep.
>
> 2.- moleculeID-1 ($r_1$) of a first branching point.
>
> 3.- moleculeID-1 ($r_2$) of a second branching point.
>
> 4.- The length ($\lambda$) of the shortest path between $r_1$ and $r_2$. This is, the number of DNAns in the shortest path.
>
> 5.- The end-to-end distance of the shortest path.
>
> 6.- The radius of gyration of the shortest path.

The, it runs the AWK script (**awk.histbin2**), which computes the probability distribution $P(\lambda)$ of observing in the simulation shortest paths made by $\lambda$ DNAns. This informations is dumped to the file **distprob**.

Using AWK to analyse the data in *fractalDimension.dat*, we create the file *avelmin\_Rgmin.dat* with four columns:

> 1.- <$\lambda$>
>
> 2.- Standard deviation of $\lambda$.
>
> 3.- <$R_{g,\lambda}$>
>
> 4.- Standard deviation of $R_{g,\lambda}$.

We also run the AWK script in **awk.ave\_at\_same\_l.sh** which creates the file **average\_l\_R\_Rg.dat** with the following columns:

> 1.- $\lambda$
>
> 2.- Average end-to-end-distance.
>
> 3.- Standard deviation of the end-to-end-distance.
>
> 4.- Average radius of gyration.
>
> 5.- Standard deviation of the radius of gyration.