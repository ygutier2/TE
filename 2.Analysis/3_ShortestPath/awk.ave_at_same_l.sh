#!/bin/bash

data_file="fractalDimension.dat"

awk '
{
    sum[$4] += $5;           # Accumulate the sum of R values (column 5) for each unique value of lambda (column 4)
    sumsq[$4] += $5 * $5;    # Accumulate the sum of R*R
    
    sumRg[$4] += $6;         # Accumulate the sum of Rg values (column 6) for each unique value of lambda (column 4)
    sumsqRg[$4] += $6 * $6;  # Accumulate the sum of Rg*Rg
    
    count[$4]++;             # Keep track of the count of values for each unique lambda
}
END {
    for (key in sum) {
        average = sum[key] / count[key];                           # Calculate the average of R
        variance = (sumsq[key] - sum[key] * sum[key] / count[key]) / count[key];  # Calculate the variance of R
        stddev = sqrt(variance);                                   # Calculate the standard deviation of R
        
        averageRg = sumRg[key] / count[key];                           # Calculate the average of Rg
        varianceRg = (sumsqRg[key] - sumRg[key] * sumRg[key] / count[key]) / count[key];  # Calculate the variance of Rg
        stddevRg = sqrt(varianceRg);                                   # Calculate the standard deviation of Rg
        
        print key, average, stddev, averageRg, stddevRg;               # Print lambda, <R>, stdevR, <Rg> and stdevRg
    }
}
' "$data_file" > "average_l_R_Rg.dat"
