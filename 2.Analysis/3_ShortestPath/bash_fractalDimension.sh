#!/bin/bash
#######################################################
## System details: needs to be filled-in by the user ##
#######################################################
  Nstars=350
  Nparticlesperstar=10
  rho=0.02
  
  #Current working directory
  cwd=$(pwd)

###########################
#Run the mathematica script
###########################
#Note that if you don't have X11 installed you will need to open the script (double click to network_connectivity.m) and run it as usual (shift+enter).
math -script MinPathBranching.m


############
#Data format
############
 echo "Data format"

 #Remove new lines found after the special character \
 sed -i '/\\$/{N;s/\n//}' shortestpath.txt

 #Remove the \ special characters
 sed -i 's/\\//g' shortestpath.txt

 #Remove all the ",{} characters
 sed -i 's/"//g' shortestpath.txt
 sed -i 's/{//g' shortestpath.txt
 sed -i 's/}//g' shortestpath.txt
 sed -i 's/,//g' shortestpath.txt

 #Remove all lines that contain a minus sign (-)
 #Rememeber that -1 appears only when there is no path between two nanostars
 sed -i '/-/d'  shortestpath.txt


##############################
#Histogram of branching points
##############################
./histogram_freq.sh


########################################################
#Compute the 3D distance (R) and Rg of the shortest path
########################################################
 echo "Computing fractal dimension"
 
 #1.- Path to the input data configurations
 datapath="../CONFIGURATIONS/rho${rho}/"
      
 #2.- Output path
 outputpath="./"
      
 #3.- Input file with atoms information (without timestep)
 rfile="stars.Nstars${Nstars}.rho${rho}."
 
 c++ fractalD.cpp -o fractalD.out -lm
 
 # READ Variables from the file created by mathematica #
 #######################################################
 awk -v dp="${datapath}" -v op="${outputpath}" -v rf="${rfile}" '{
   arguments = dp" "op" "rf;
   
   for (i = 1; i <= NF; i++) {
     col[i] = $i;
     arguments = arguments" "col[i];     
   }
   
   #print "Arguments:", arguments;
   # Execute the C++ program and pass the concatenated values as a single argument
   cmd = "./fractalD.out " arguments;
   system(cmd);
 }' shortestpath.txt
 
      
################
#Compute P(lmin)
################
 echo "Computing P(lmin)"
   
 # minimum value of x axis
 m="0"
  
 # maximum value of x axis
 M="100"
  
 # number of bins
 b="100"

 # output file for plotting
 fout="distprob"
  
 # input file
 fin="fractalDimension.dat"
  
 gawk -f awk.histbin2 ${m} ${M} ${b} ${fout} ${fin}
 
##########################
#Compute <lmin> and <Rg,l>
##########################
 
 awk '{aveLmin+=$4; aveRgmin+=$6; i++; deltaL=$4-avgL; avgL+=deltaL/NR; meanL2+=deltaL*($4-avgL); deltaR=$6-avgR; avgR+=deltaR/NR; meanR2+=deltaR*($6-avgR);} END { print aveLmin/i,sqrt(meanL2/NR), aveRgmin/i,sqrt(meanR2/NR); }' fractalDimension.dat > avelmin_Rgmin.dat


################################################################
#Compute average of R and Rg at the same l from the scatter plot
################################################################
 echo "Average Rg at same l"
 chmod a+x awk.ave_at_same_l.sh
 ./awk.ave_at_same_l.sh

 echo "Finish of the analysis"
