# Largest connected component
The script **bash\_maxCC.sh** contains the instructions to run the whole analysis in this folder:

**I.-** First, it executes the mathematica notebook **maxCComp.m** to read the network connectivity files produced in folder **1\_networkConnectivity/REP1/**. These files contain information about which DNAns (with  moleculeID $m\in[0,N-1]$) is connected with any other DNAns. By runnig the mathematica notebook, you will create a file called *maxCC.txt* with columns:

* 1.- timestep.
* 2.- The size (number of DNAns) in the largest connected component.

**Note:** if you have problems running the mathematica script through the bash, you will need to open the script (maxCComp.m) and run it as usual (shift+enter) to create the previous file. Then continue running bash\_maxCC.sh.

**II.-** Then, it removes the special characters *"* from the file *maxCC.txt* and rename it to: *maxCComp_rho0.02.txt*

**Note:** The variables inside the mathematica notebook: The number (M) of nanostars, and the initial time (ti), time-frequency (dfreq) and last timestep (tl), need to be set accordingly to the system you are analysing.
