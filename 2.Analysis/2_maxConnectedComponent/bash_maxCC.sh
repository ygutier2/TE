#!/bin/bash
#######################################################
## System details: needs to be filled-in by the user ##
#######################################################
  Nstars=350
  Nparticlesperstar=10
  rho=0.02
  alpha=120
  
####################################################
## Execute the mathematica script in the terminal ##
####################################################
#Note that if you don't have X11 installed you will need to open the script (double click to network_connectivity.m) and run it as usual (shift+enter).
math -script maxCComp.m

############
#Data format
############
#Remove all the " characters
sed -i 's/"//g' maxCComp.txt

#Change the file name
mv maxCComp.txt maxCComp_rho${rho}.txt
