(* ::Package:: *)

(* ::Input::Initialization:: *)
(*Uncomment next line if running from Graphical Interface*)

(*SetDirectory[NotebookDirectory[]];*)

(***********)
(*Variables*)
(***********)
(*Number of DNAns*)
M=350;
(*Initial time, dump frequency and last timestep*)
ti=0;
dfreq=100000;
tf=1000000;
frames=(tf-ti)/dfreq+1;


(* ::Input::Initialization:: *)
(************************************************)
(*Analyse data from different snapshots in a loop*)
(************************************************)
For[t=0,t<frames,t++,
timestep=ti+t*dfreq;
data=Import["../1_networkConnectivity/REP1/network_connectivity_t"<>ToString[timestep]<>"_Nstars"<>ToString[M]<>"_rho_0.02_REP1.dat","Table"];

(*Molecules (stars) that are connected*)
datamol=Table[data[[i]][[1]]\[UndirectedEdge]data[[i]][[2]],{i,1,Length[data]}];
gmol=Graph[datamol,VertexLabels->None];
 (*The connected components in the network*)
ConnectedComponents[gmol];
(*Size of largest connected component*)
lmaxcc=Length[ConnectedComponents[gmol][[1]]];
If[lmaxcc>=0,
PutAppend[StringRiffle[{timestep,lmaxcc}],"maxCComp.txt"];
,
(*If lmaxcc is not larger than 0*)
Continue[]](*Close if statement on l*)
](*Close loop on time*)
