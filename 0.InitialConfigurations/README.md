# Initial configurations

In this folder you will find different configurations that have been previously equilibrated and that are written in a format compatible with LAMMPS. For instance, the file "Equilibrated.Nstars350.rho0.02.data" represents a system in which:

> 1.-We simulate $N=350$ DNAns, each one made of 7 beads and 3 patches (10 particles), so there are 3500 particles in the system.
>
> 2.-The size of the box is $L=40\sigma$, so the volume fraction of beads is $\rho=N V_{1}/L^{3}=0.02$, where $V_{1}=7 \frac{4}{3}\pi(\sigma/2)^{3}$ is the excluded volume of one nanostar, given by the sum of the volume of all its structural beads.
>
> 3.-Beads are type 1 and interact via a purely repulsive Lennard-Jones potential.
>
> 4.-Patches are type 2 and interact via a Morse potential.
>
> 5.-If $m \in [1,N]$ represents the molecule-ID of nanostars in the system, we set simulations such as:
>
>>  a) Nanostars' cores have particle-ID: $10m$.
>>
>>  b) Patches have particle-ID: $10(m-1)+3$, $10(m-1)+6$ and $10(m-1)+9$.
>>
>>  c) The rest of particle-IDs correspond to beads representing the nanostar arms.

We note that to obtain the equilibrated simulations in this folder, we first performed an initial run in which we turned-off the attraction between patches (the energy of the morse potential is set to $\epsilon_{m}=0 k_{B}T$) and integrate the system for $5\times10^{7}$ timesteps. We then set the attraction of patches to $\epsilon_{m}=25 k_{B}T$, allowing in this way the network formation, and we ran the system for $10^{8}$ timesteps. The file obtained at the end of this run is the one provided here.

In the Figure below we show a sketch of a DNAns in our coarse-grained model, the interaction between two nanostars and a snapshot of the configuration at the last timestep from simulations.

|![](sketch.png)|
|:--:| 
|*Figure 1: The coarse-grained model*| 
|*(a) Schematic representation of the coarse-grained model of a single DNA nanostar. Beads are depicted in green, while patches are depicted in pink. (b) Sketch of the attractive interaction between two DNAns. Numbers represent the particle IDs in the simulation. The first molecule has moleculeID m=1, and patches have particleIDs=3,6,9. The second molecule has m=5 and patches IDs 43,46,49. The two molecules are hybridized through the interaction between patches 6 and 49. (c) Snapshot from simulations at $\rho=0.02$.*|
       


